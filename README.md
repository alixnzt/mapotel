# MapOtel

## Installation du projet
```
npm install
```

### Compilation pour developpement
```
npm run serve
```

## Utilisation de l'application
Après avoir installer et lancer l'application, elle se lance sur (http://localhost:8080/).

L'application affiche une liste des hotels les plus proche par rappport au centre de la map.

Pour afficher la liste d'hotel pour la première fois il faut bouger la carte 2/3 fois pour actualiser cette dernière ou bien rechercher une adresse dans la barre de recherche à gauche de l'écran.
